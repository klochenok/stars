from django.db import models
from django.utils import timezone


class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    class Meta:
        db_table = 'authors'

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    isbn = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    publish_date = models.DateField(default=timezone.now)

    class Meta:
        db_table = 'books'
