from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template.context_processors import csrf
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from easyaudit.models import CRUDEvent, RequestEvent

from catalog.forms import BookForm
from catalog.models import Book


class BookListView(ListView):
    model = Book

    def get_ordering(self):
        """Return the field or fields to use for ordering the queryset."""
        order = self.request.GET.get('order')
        if order and order == 'latest':
            self.ordering = '-publish_date'
        elif order and order == 'oldest':
            self.ordering = 'publish_date'
        return self.ordering

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        return self.render_to_response(context)


class CreateBookView(CreateView):
    model = Book
    fields = '__all__'


class UpdateBookView(UpdateView):
    template_name = 'catalog/book_detail.html'

    def get_book_by_id(self):
        book_id = self.kwargs.get('pk')
        book = get_object_or_404(Book, id=book_id)
        return book

    def get(self, request, *args, **kwargs):
        book = self.get_book_by_id()
        form = BookForm(
            initial={
                'title': book.title,
                'author': book.author,
                'isbn': book.isbn,
                'price': book.price,
                'publish_date': book.publish_date}
        )
        context = {'form': form}
        context.update(csrf(request))
        return render_to_response(self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        book = self.get_book_by_id()
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            form.save()
        return redirect('/')


class DeleteBookView(DeleteView):
    model = Book
    success_url = reverse_lazy('book_list')


class RequestsListView(ListView):
    queryset = RequestEvent.objects.all()[:10]
    template_name = 'catalog/request_list.html'


class ChangesListView(ListView):
    queryset = CRUDEvent.objects.all()
    template_name = 'catalog/changes_list.html'

    def get(self, request, *args, **kwargs):
        content_type = get_object_or_404(ContentType, model='book',
                                         app_label='catalog')
        self.object_list = self.get_queryset()
        self.object_list = self.object_list.filter(
            content_type=content_type)[:10]
        context = self.get_context_data()
        return self.render_to_response(context)
