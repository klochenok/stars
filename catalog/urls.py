from django.urls import re_path

from .views import (
    BookListView,
    ChangesListView,
    CreateBookView,
    DeleteBookView,
    RequestsListView,
    UpdateBookView
)

urlpatterns = [
    re_path(r'^books/$', BookListView.as_view(), name='book_list'),
    re_path(r'^book/new/$', CreateBookView.as_view(success_url="/"),
            name='book_new'),
    re_path(r'^book/(?P<pk>\d+)/update/$', UpdateBookView.as_view(),
            name='book_edit'),
    re_path(r'^book/(?P<pk>\d+)/delete/$', DeleteBookView.as_view(),
            name='book_delete'),
    re_path(r'^requests/$', RequestsListView.as_view(), name='request_list'),
    re_path(r'^changes/$', ChangesListView.as_view(), name='changes_list')
]
