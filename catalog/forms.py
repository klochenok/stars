from django import forms
from django.forms.widgets import SelectDateWidget

from .models import Book


class BookForm(forms.ModelForm):
    publish_date = forms.DateField(widget=SelectDateWidget)

    class Meta:
        model = Book
        fields = '__all__'
