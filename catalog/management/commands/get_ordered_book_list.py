from django.core.management.base import BaseCommand

from catalog.models import Book


class Command(BaseCommand):

    help = ('Return list of books with possibility to order by publish date by'
            ' defining ordering')

    def add_arguments(self, parser):
        parser.add_argument(
            '-d', '--desc',
            action='store_true',
            dest='desc',
            help='Show list of books with desc ordering.')
        parser.add_argument(
            '-a', '--asc',
            action='store_true',
            dest='asc',
            help='Show list of books with asc ordering.')

    def handle(self, *args, **options):
        unordered_queryset = Book.objects.all().values('title', 'publish_date')
        if options['desc']:
            book_list = unordered_queryset.order_by('-publish_date')
        elif options['asc']:
            book_list = unordered_queryset.order_by('publish_date')
        else:
            book_list = unordered_queryset
        for book in book_list:
            self.stdout.write("Title: {}, publish date: {}".format(
                book['title'],
                book['publish_date'])
            )
